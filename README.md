# iaac-poc

PoC for IaaC

I want to use this repository to create simple cluster on GCP and document the way it was created.


| Variable     | Value     | Description                    |
|--------------|-----------|--------------------------------|
| PROJECT_ID   | iaac-poc  | The ID of the hosting project. |
| DEPLOYMENT   | iaac-deployment  | Name of the deployment  |
| CLUSTER_NAME   | aks-cluster  | Name of the created cluster  |
| ZONE   | europe-west3-b  | GCP cloud zone  |
| NODE_COUNT   | 3  | Number of node in the cluster  |
| SA_KEY   |  sa_key.json | Key for authenticating the service account  |

Except for `PROJECT_ID` sample values are provided in the `.env` file.

## Project

First, we login:

    $ gcloud auth login

We will use the GCP tools to create a project:

    $ gcp project create <PROJECT_ID>

Switch to the newly created project

    $ gcloud config set project <PROJECT_ID>

Please make sure to enable following APIs:

    $ gcloud services enable deploymentmanager.googleapis.com
    $ gcloud services enable container.googleapis.com

When we are finish we can delete the project:

    $ gcp project delete <PROJECT_ID>



## Cloud Deployment Manager

We are using Cloud Deployment Manager, service provided by Google to create resources.

Set some ENV variables first (I use fish shell)

    $ set -x DEPLOYMENT test-demo-app
    $ set -x ZONE europe-west3-b // Frankfurt

Alternatively use `dotenv` command to read ENV vars from a `.env` file.

    $ dotenv

### Generate

The final template (need to have `envsubst` command installed):

    $ envsubst < resources/cluster.yaml.tpl > resources/cluster.yaml

Deploy:

    $ gcloud deployment-manager deployments create $DEPLOYMENT --config=resources/cluster.yaml

List:

    $ gcloud deployment-manager deployments list

Destroy the cluster once you are done:

    $ gcloud deployment-manager deployments delete $DEPLOYMENT

Connect to tu cluster with:

    $ gcloud container clusters get-credentials ${NAME}-cluster-py --zone ${ZONE}
    $ kubectl get deployments
    $ kubectl get services

### Additional resources

This command creates new `Deployment` on the generated cluster.

    $ cloud deployment-manager deployments create sample-app-test-deployment --config resources/deployment.yaml

## Service Accounts

In order to create the cluster from the pipeline, we need to create Service Account with proper permissions.

Create new service account

    $ gcloud iam service-accounts create iaac-poc-cicd --display-name="Service Account to handle CICD jobs"

Verify this was create:

    $ gcloud iam service-accounts list
    DISPLAY NAME                            EMAIL                                               DISABLED
    Compute Engine default service account  943295490020-compute@developer.gserviceaccount.com  False
    iaac-poc-cicd                           iaac-poc-cicd@iaac-poc.iam.gserviceaccount.com      False

Grant all necesary roles, we will need those:

| Role     |
|--------------|
| roles/compute.serviceAgent  |
| roles/container.serviceAgent  |
| roles/containerregistry.serviceAgent  |
| roles/deploymentmanager.editor  |
| roles/viewer  |


    $ gcloud projects add-iam-policy-binding iaac-poc --member=serviceAccout:iaac-poc-cicd@iaac-poc.iam.gserviceaccount.com --role=roles/compute.serviceAgent
    $ gcloud projects add-iam-policy-binding iaac-poc --member=serviceAccout:iaac-poc-cicd@iaac-poc.iam.gserviceaccount.com --role=roles/container.serviceAgent
    $ gcloud projects add-iam-policy-binding iaac-poc --member=serviceAccout:iaac-poc-cicd@iaac-poc.iam.gserviceaccount.com --role=roles/containerregistry.serviceAgent
    $ gcloud projects add-iam-policy-binding iaac-poc --member=serviceAccout:iaac-poc-cicd@iaac-poc.iam.gserviceaccount.com --role=roles/deploymentmanager.editor
    $ gcloud projects add-iam-policy-binding iaac-poc --member=serviceAccout:iaac-poc-cicd@iaac-poc.iam.gserviceaccount.com --role=roles/viewer

Verify everything is ax expected and generate privat key for authenticating the service account.

    $ gcloud projects get-iam-policy ${PROJECT_ID}
    $ gcloud iam service-accounts keys create ${SA_KEY}  --iam-account=iaac-poc-cicd@iaac-poc.iam.gserviceaccount.com


## Gitlab

We are using Gitlab as tool for storing files and CICD pipelines.


## Sources

- [Deployment Manager with Type Providers](https://medium.com/google-cloud/2018-google-deployment-manager-5ebb8759a122)
- [GCP Infrastructure as Code with Deployment Manager](https://medium.com/google-cloud/2018-google-deployment-manager-5ebb8759a122)
- [GCP Goodies Part 1 — Google Deployment Manager Basics](https://blog.softwaremill.com/gcp-goodies-part-1-google-deployment-manager-basics-747ce637e61b)
- [GCP Goodies Part 2— Google Deployment Manager with Kubernetes Type Provider](https://blog.softwaremill.com/gcp-goodies-part-2-google-deployment-manager-with-kubernetes-type-provider-fcc2c2d80422)
- [GCP Goodies Part 3— Google Deployment Manager — Type Providers with Custom API](https://blog.softwaremill.com/gcp-goodies-part-3-google-deployment-manager-type-providers-with-custom-api-cc13190b018e)
- [Deployment manager examples](https://github.com/GoogleCloudPlatform/deploymentmanager-samples)
- [Google Service Accounts](https://cloud.google.com/iam/docs/creating-managing-service-accounts)
- [Datadog Helm Charts](https://github.com/DataDog/helm-charts/tree/main/examples/datadog)