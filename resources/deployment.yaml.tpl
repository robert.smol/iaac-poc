imports:
- path: deployment.py

resources:
- name: nginx
  type: deployment.py
  properties:
    clusterType: $CLUSTER_TYPE
    image: $IMAGE
    namespace: $NAMESPACE
    port: $PORT